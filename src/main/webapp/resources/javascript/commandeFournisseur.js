$(document).ready(function() {
	$("#codeArticle_search").on("keypress", function(e) {
		if(e.which == '13') {
			var codeArticle = $("#codeArticle_search").val();
			if(verifierFournisseur() && codeArticle) {
				searchArticle(codeArticle);
			}
		}
	});
	
	$("#listFournisseurs").on("change", function(e) {
		if(verifierFournisseur()) {
			$("#fournisseurNotSelectedMsgBlock").hide("slow", function() {$("#fournisseurNotSelectedMsgBlock").hide()});
			creerCommande($("#listFournisseurs option:selected").val());
		}
	});
	$("#btnEnrigtrerCommande").on("click", function() {
		$.getJSON("enrigstrerCommande", function(data) {
			if (data) {
				window.location=""+data;
			}
		});
	});
	
	$("#notFoundMsgBlock").hide();
	$("#fournisseurNotSelectedMsgBlock").hide();
	$("#unexpectedErrorMsgBlock").hide();
	
});

function verifierFournisseur() {
	var idFournisseur = $("#listFournisseurs option:selected").val();
	if(idFournisseur) {
		if (idFournisseur === "-1") {
			$("#fournisseurNotSelectedMsgBlock").show("slow", function() {$("#fournisseurNotSelectedMsgBlock").show()});
			return false;
		}
		return true;
	}
}

function creerCommande(idFournisseur) {
	if(idFournisseur) {
		$.getJSON("creerCommande", {
			idFournisseur: idFournisseur,
			ajax: true
		},
		function(data) {
			console.log("fournisseur a ete mis a jour");
		});
	}
}

function updateDetailCommande(idCommande) {
	var json = $.parseJSON($("#json" + idCommande).text());
	var detailHtml = ""+idCommande;
	if(json) {
		for(var index = 0; index < json.length; index++) {
			detailHtml += 
						"<tr>"+
							"<td>" + json[index].article.codeArticle + "</td>"+
						"</tr>";
		}
		$("#detailCommande").html(detailHtml);
	}	
}

function searchArticle(codeArticle) {
	if(codeArticle) {
		var detailHtml = "";
		$.getJSON("ajouterLigne", {
			codeArticle: codeArticle,
			ajax: true
		}, 
		function(data) {
			if(data) {
				var total = data.quantite * data.prixUnitaire;
				if($("#qte" + data.article.idArticle).length > 0 && $("#total" + data.article.idArticle).length > 0) {
					$("#qte" + data.article.idArticle).text(data.quantite);
					$("#total" + data.article.idArticle).text(total);
				} else {
					detailHtml += 
						"<tr id='ligne" + data.article.idArticle + "'>"+
							"<td>" + data.article.codeArticle + "</td>"+
							"<td id='qte" + data.article.idArticle + "'>" + data.quantite + "</td>"+
							"<td>" + data.prixUnitaire + "</td>"+
							"<td id='total" + data.article.idArticle + "'>" + total + "</td>"+
							"<td ><button class='btn btn-link' onclick='supprimerLigneCommande(" + data.article.idArticle + ")'><i class='fa fa-trash-o'></i></button></td>"+
						"</tr>";
					$("#detailNouvelleCommande").append(detailHtml);
				}
				$("#notFoundMsgBlock").hide("slow", function() {$("#notFoundMsgBlock").hide()});
				$("#codeArticle_search").val("");
			}
		}).fail(function() {
			$("#notFoundMsgBlock").show("slow", function() {$("#notFoundMsgBlock").show()});
		});
	}	
}


function supprimerLigneCommande(idArticle) {
	if($("#ligne" + idArticle).length > 0) {
		$.getJSON("supprimerLigne", {
			idArticle: idArticle,
			ajax: true
		},
		function(data){
			if(data) {
				$("#ligne" + idArticle).hide("slow", function() {$("#ligne" + idArticle).remove()});
			}
		});
	}
}