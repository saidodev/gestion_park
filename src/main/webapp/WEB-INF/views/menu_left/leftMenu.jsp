<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li class="sidebar-search">
				<div class="input-group custom-search-form">
					<input type="text" class="form-control" placeholder="Recherche...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div> <!-- /input-group -->
			</li>
			<c:url value="/home/" var="home" />
			<li class=""><a href="${home }"><i class="fa fa-tachometer" aria-hidden="true"></i>
					Tabeau de bord </a></li>
					
		
					
			<li><a href="#"><i class="fa fa-car"></i>
					Gestion Vehicule<span class="fa arrow"></span></a>
					
				<ul class="nav nav-second-level" area-expended="false">
				
					<c:url value="/conducteur/" var="conducteur" />
					<li><a href="${conducteur }">Conducteur</a></li>
					
					<c:url value="/departement/" var="departement" />
					<li><a href="${departement }">Departement</a></li>
					
					
					<c:url value="/vehicule/" var="vehicule" />
					<li><a href="${vehicule }">vehicule</a></li>
					
					
					<c:url value="/ville/" var="ville" />
					<li><a href="${ville }">Ville</a></li>
					
					
					<c:url value="/achat/" var="achat" />
					<li><a href="${achat }">Achat/Location</a></li>
					
					<c:url value="/fournisseur/" var="fournisseur" />
					<li><a href="${fournisseur }">Fournisseur</a></li>
				</ul> <!-- /.nav-second-level --></li>
				
				
				<li><a href="#"><i class="fa fa-wrench">  </i>
					Entretien<span class="fa arrow"></span></a>
					
				<ul class="nav nav-second-level" area-expended="false">
				
					<c:url value="/entretien/" var="entretien" />
					<li><a href="${entretien }">Entretiens</a></li>
					
					<c:url value="/entretien/" var="entretien" />
					<li><a href="${entretien }">Entretiens Maitres</a></li>
					
					
					<c:url value="/entretien/" var="entretien" />
					<li><a href="${entretien }">Programme Entretien</a></li>
					
				</ul> <!-- /.nav-second-level --></li>
				
				<li><a href="#"><i><img  src="<%=request.getContextPath()%>/resources/vendor/gas-station.png"></i>
					Carburant<span class="fa arrow"></span></a>	
				<ul class="nav nav-second-level" area-expended="false">
				
					<c:url value="/conducteur/" var="conducteur" />
					<li><a href="${conducteur }">Plein Essence</a></li>
					
				</ul> <!-- /.nav-second-level --></li>
				
				<li><a href="#"><i class="fa fa-bar-chart"></i>
					Statistiques<span class="fa arrow"></span></a>	
				<ul class="nav nav-second-level" area-expended="false">
				
					<c:url value="/conducteur/" var="conducteur" />
					<li><a href="${conducteur }">Rapport Depenses</a></li>
					
					<c:url value="/conducteur/" var="conducteur" />
					<li><a href="${conducteur }">Rapport Depenses Global</a></li>
					
					<c:url value="/conducteur/" var="conducteur" />
					<li><a href="${conducteur }">Carburant Mensuel</a></li>
					
					<c:url value="/conducteur/" var="conducteur" />
					<li><a href="${conducteur }">Carburant Semestrielle</a></li>
					
					<c:url value="/conducteur/" var="conducteur" />
					<li><a href="${conducteur }">Entretien Reparation Mensuel</a></li>
					
					<c:url value="/conducteur/" var="conducteur" />
					<li><a href="${conducteur }">Rapport Global</a></li>
					
				</ul> <!-- /.nav-second-level --></li>
				
				
				<li><a href="#"><i class="fa fa-cog"></i>
					Administration<span class="fa arrow"></span></a>	
				<ul class="nav nav-second-level" area-expended="false">
				
					<c:url value="/utilisateur/" var="utilisateur" />
					<li><a href="${utilisateur }">Utilsateur</a></li>
					
				</ul> <!-- /.nav-second-level --></li>
						
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>