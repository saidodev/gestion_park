<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>

<%@ page session="true" %>
<%@ page trimDirectiveWhitespaces="true" %>

<fmt:setLocale value="{$locale}"/>
<fmt:bundle basename="i18n.applicationresources_fr.properties"/>
 
