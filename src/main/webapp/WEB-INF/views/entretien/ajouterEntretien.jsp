<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<!DOCTYPE html>
<html lang="fr">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gestion de Park</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath() %>/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath() %>/resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath() %>/resources/dist/css/sb-admin-2.css" rel="stylesheet">
    
    <!-- DataTables CSS -->
    <link href="<%=request.getContextPath() %>/resources/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<%=request.getContextPath() %>/resources/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath() %>/resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            
            <%@ include file="/WEB-INF/views/menu_top/topMenu.jsp" %>
			
            <%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp" %>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Nouveau Entretien</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                
               
                
                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Nouveau Entretien
                        </div>
                        <!-- /.panel-heading -->
                      
                       <div class="panel-body">
                         <c:url value="/entretien/enregistrer" var ="urlEnregistrer" />
                         	<f:form modelAttribute="entretien" action="${urlEnregistrer }">
                         		<f:hidden path="id_entretien"/>
                         		
                                    <div class="form-group">
                                        <label>Cout</label>
                                        <f:input type="text" path="cout" class="form-control" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Date</label>
                                        <f:input type="date" path="date" class="form-control" />
                                    </div>
                                    
                                    
                                      <div class="form-group">
                                        <label>Description</label>
                                        <f:textarea  path="description" class="form-control" placeholder="description..." />
                                    </div>
                                    
                              
                                    
                                    <div class="form-group">
                                        <label>Vehicule</label>
                                        
                                         <f:select class="form-control" path="vehicule.id_vehicule" items="${vehicules}" itemLabel="matricule" itemValue="id_vehicule" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Fournisseur</label>
                                        
                                         <f:select class="form-control" path="fournisseur.id_fournisseur" items="${fournisseurs}" itemLabel="nom_fournisseur" itemValue="id_fournisseur" />
                                    </div>
                      
                                    
                                    <div class="panel-footer">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save">&nbsp;enregister</i></button>
                                        <a href="<c:url value="/entretien/" />" class="btn btn-danger"> <i class="fa fa-arrow-left">&nbsp;annuler</i></a>
                                 </div>
                                    
                         	</f:form>
                       
                       </div>
                      
                      
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<%=request.getContextPath() %>/resources/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<%=request.getContextPath() %>/resources/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<%=request.getContextPath() %>/resources/vendor/metisMenu/metisMenu.min.js"></script>

    
    <!-- DataTables JavaScript -->
    <script src="<%=request.getContextPath() %>/resources/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<%=request.getContextPath() %>/resources/dist/js/sb-admin-2.js"></script>
    
    <!-- Custom Theme JavaScript -->
    <script src="<%=request.getContextPath() %>/resources/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
</body>

</html>
