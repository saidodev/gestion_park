package com.said.gestionpark.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Fournisseur implements Serializable {
	@Id
	@GeneratedValue
	private Long id_fournisseur;
	
	private String nom_fournisseur;
	
	private String type;
	
	private String adresse;
	
	@ManyToOne
	@JoinColumn(name = "id_ville")
	private Ville ville;
	
	

	private int codepostal;
	
	private String telephone;
	
	private String fax;
	
	private String email;
	
	private String site_web;
	
	@OneToMany(mappedBy = "fournisseur")
	public List<Achat> achat;
	
	@OneToMany(mappedBy = "fournisseur")
	public List<Entretien> entretien;

	
	public Fournisseur() {
		// TODO Auto-generated constructor stub
	}

	public Long getId_fournisseur() {
		return id_fournisseur;
	}

	public void setId_fournisseur(Long id_fournisseur) {
		this.id_fournisseur = id_fournisseur;
	}

	public String getNom_fournisseur() {
		return nom_fournisseur;
	}

	public void setNom_fournisseur(String nom_fournisseur) {
		this.nom_fournisseur = nom_fournisseur;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}

	public int getCodepostal() {
		return codepostal;
	}

	public void setCodepostal(int codepostal) {
		this.codepostal = codepostal;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSite_web() {
		return site_web;
	}

	public void setSite_web(String site_web) {
		this.site_web = site_web;
	}

	public List<Achat> getAchat() {
		return achat;
	}

	public void setAchat(List<Achat> achat) {
		this.achat = achat;
	}
	
	
	
	
	
	
	

}
