package com.said.gestionpark.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Vehicule implements Serializable{
	
	@Id
	@GeneratedValue
	private Long id_vehicule;
	
	private String matricule;
	

	private String marque;
	
	private String model;
	
	private String couleur;
	
	private Long numero_serie;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date mise_circulation;
	
	private String carburant;
	
	private boolean etat;
	
	@ManyToOne
	@JoinColumn(name = "id_departement")
	private Departement departement;
	
	@OneToOne
	@JoinColumn(name = "id_conducteur")
	private Conducteur conducteur;
	

	@OneToMany(mappedBy = "vehicule")
	public List<Achat> achat;
	
	@OneToMany(mappedBy = "vehicule")
	public List<Entretien> entretien;
	
	

	public Vehicule() {
		// TODO Auto-generated constructor stub
	}

	public Long getId_vehicule() {
		return id_vehicule;
	}

	public void setId_vehicule(Long id_vehicule) {
		this.id_vehicule = id_vehicule;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}
	
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public Long getNumero_serie() {
		return numero_serie;
	}

	public void setNumero_serie(Long numero_serie) {
		this.numero_serie = numero_serie;
	}

	public Date getMise_circulation() {
		return mise_circulation;
	}

	public void setMise_circulation(Date mise_circulation) {
		this.mise_circulation = mise_circulation;
	}

	public String getCarburant() {
		return carburant;
	}

	public void setCarburant(String carburant) {
		this.carburant = carburant;
	}

	public boolean isEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public Conducteur getConducteur() {
		return conducteur;
	}

	public void setConducteur(Conducteur conducteur) {
		this.conducteur = conducteur;
	}

	public List<Achat> getAchat() {
		return achat;
	}

	public void setAchat(List<Achat> achat) {
		this.achat = achat;
	}

	public List<Entretien> getEntretien() {
		return entretien;
	}

	public void setEntretien(List<Entretien> entretien) {
		this.entretien = entretien;
	}
	
	
	
	
	
	
	
	

}
