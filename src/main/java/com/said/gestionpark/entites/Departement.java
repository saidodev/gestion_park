package com.said.gestionpark.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Departement implements Serializable {
	
	@Id
	@GeneratedValue
	private Long id_departement;
	
	private String nom_departement;
	
	@OneToMany(mappedBy = "departement")
	public List<Vehicule> vehicule;

	
	
	public List<Vehicule> getVehicule() {
		return vehicule;
	}

	public void setVehicule(List<Vehicule> vehicule) {
		this.vehicule = vehicule;
	}

	public Departement() {
		// TODO Auto-generated constructor stub
	}

	public Long getId_departement() {
		return id_departement;
	}

	public void setId_departement(Long id_departement) {
		this.id_departement = id_departement;
	}

	public String getNom_departement() {
		return nom_departement;
	}

	public void setNom_departement(String nom_departement) {
		this.nom_departement = nom_departement;
	}
	
	

}
