package com.said.gestionpark.entites;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Ville implements Serializable{
	
	@Id
	@GeneratedValue
	private Long id_ville;
	
	private String nom_ville;
	
	@OneToMany(mappedBy = "ville")
	public List<Conducteur> conducteurs;
	
	@OneToMany(mappedBy = "ville")
	public List<Fournisseur> fournisseurs;
	


	

	public List<Conducteur> getConducteurs() {
		return conducteurs;
	}

	public void setConducteurs(List<Conducteur> conducteurs) {
		this.conducteurs = conducteurs;
	}

	public Long getId_ville() {
		return id_ville;
	}

	public void setId_ville(Long id_ville) {
		this.id_ville = id_ville;
	}

	public String getNom_ville() {
		return nom_ville;
	}

	public void setNom_ville(String nom_ville) {
		this.nom_ville = nom_ville;
	}
	
	
	

}
