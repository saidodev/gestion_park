package com.said.gestionpark.entites;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Entretien {

	@Id
	@GeneratedValue
	private Long id_entretien;
	
	private float cout;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "id_vehicule")
	private Vehicule vehicule;
	
	@ManyToOne
	@JoinColumn(name = "id_fournisseur")
	private Fournisseur fournisseur;

	public Long getId_entretien() {
		return id_entretien;
	}

	public void setId_entretien(Long id_entretien) {
		this.id_entretien = id_entretien;
	}

	public float getCout() {
		return cout;
	}

	public void setCout(float cout) {
		this.cout = cout;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Vehicule getVehicule() {
		return vehicule;
	}

	public void setVehicule(Vehicule vehicule) {
		this.vehicule = vehicule;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}
	
	
	
	
	
	
}
