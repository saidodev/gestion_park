package com.said.gestionpark.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
public class Conducteur implements Serializable{
	
	@Id
	@GeneratedValue
	private Long id_conducteur;
	
	private String nom;
	
	private String prenom;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	
	private Date date_naissance;
	
	@Temporal(TemporalType.TIMESTAMP)
	
	private Date date_embauche;
	
	
	private String numero_permis;
	
	private String adresse;
	
	@ManyToOne
	@JoinColumn(name = "id_ville")
	private Ville ville;
	
	private Long codepostal;
	
	private String telephone;
	
	private String email;
	
	@OneToOne
	@JoinColumn(name = "id_vehicule")
	private Vehicule vehicule;
	
	
	
	public Date getDate_naissance() {
		return date_naissance;
	}

	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}

	public Date getDate_embauche() {
		return date_embauche;
	}

	public void setDate_embauche(Date date_embauche) {
		this.date_embauche = date_embauche;
	}

	
	
	public Conducteur() {
		// TODO Auto-generated constructor stub
	}

	public Long getId_conducteur() {
		return id_conducteur;
	}

	public void setId_conducteur(Long id_conducteur) {
		this.id_conducteur = id_conducteur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	

	public String getNumero_permis() {
		return numero_permis;
	}

	public void setNumero_permis(String numero_permis) {
		this.numero_permis = numero_permis;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}

	public Long getCodepostal() {
		return codepostal;
	}

	public void setCodepostal(Long codepostal) {
		this.codepostal = codepostal;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
