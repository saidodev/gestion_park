package com.said.gestionpark.entites;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Achat {
	
	@Id
	@GeneratedValue
	private Long id_achat;
	
	@Temporal(TemporalType.TIMESTAMP)
	
	private Date date_achat;
	
	private Long kilometrage;
	
	private Long prix;
	
	private Long prix_vente;
	
	private boolean location;

	
	public void setLocation(boolean location) {
		this.location = location;
	}

	@Temporal(TemporalType.TIMESTAMP)
	private Date date_expiration;
	
	
	@ManyToOne
	@JoinColumn(name = "id_vehicule")
	private Vehicule vehicule;
	
	@ManyToOne
	@JoinColumn(name = "id_fournisseur")
	private Fournisseur fournisseur;

	public Long getId_achat() {
		return id_achat;
	}

	public void setId_achat(Long id_achat) {
		this.id_achat = id_achat;
	}

	public Date getDate_achat() {
		return date_achat;
	}

	public void setDate_achat(Date date_achat) {
		this.date_achat = date_achat;
	}

	public Long getKilometrage() {
		return kilometrage;
	}

	public void setKilometrage(Long kilometrage) {
		this.kilometrage = kilometrage;
	}

	public Long getPrix() {
		return prix;
	}

	public void setPrix(Long prix) {
		this.prix = prix;
	}

	public Long getPrix_vente() {
		return prix_vente;
	}

	public void setPrix_vente(Long prix_vente) {
		this.prix_vente = prix_vente;
	}



	public boolean isLocation() {
		return location;
	}

	public Date getDate_expiration() {
		return date_expiration;
	}

	public void setDate_expiration(Date date_expiration) {
		this.date_expiration = date_expiration;
	}

	public Vehicule getVehicule() {
		return vehicule;
	}

	public void setVehicule(Vehicule vehicule) {
		this.vehicule = vehicule;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}


	
	
	
	
	
	
	
	
	

}
