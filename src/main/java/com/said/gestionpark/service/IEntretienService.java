package com.said.gestionpark.service;

import java.util.List;

import com.said.gestionpark.entites.Entretien;


public interface IEntretienService {
	
	  public Entretien save(Entretien entity);
		
		public Entretien update(Entretien entity);

		public List<Entretien> selectAll();

		public List<Entretien> selectAll(String sortField, String sort);

		public Entretien getById(Long id);

		public void remove(Long id);

		public Entretien findOne(String paramName, Object paramValue);

		public Entretien findOne(String[] paramNames, Object[] paramValues);

		public int findCountBy(String paramName, String paramValue);

}
