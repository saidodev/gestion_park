package com.said.gestionpark.service;

import java.util.List;

import com.said.gestionpark.entites.Conducteur;



public interface IConducteurService {
	
	public Conducteur save(Conducteur entity);
	
	public Conducteur update(Conducteur entity);

	public List<Conducteur> selectAll();

	public List<Conducteur> selectAll(String sortField, String sort);

	public Conducteur getById(Long id);

	public void remove(Long id);

	public Conducteur findOne(String paramName, Object paramValue);

	public Conducteur findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);


}
