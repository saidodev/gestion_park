package com.said.gestionpark.service;

import java.util.List;

import com.said.gestionpark.entites.Conducteur;
import com.said.gestionpark.entites.Ville;

public interface IVilleService {
	
public Ville save(Ville entity);
	
	public Ville update(Ville entity);

	public List<Ville> selectAll();

	public List<Ville> selectAll(String sortField, String sort);

	public Ville getById(Long id);

	public void remove(Long id);

	public Ville findOne(String paramName, Object paramValue);

	public Ville findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

	List<Conducteur> selectAllConducteurByVille(Long id_ville);

}
