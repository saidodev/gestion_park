package com.said.gestionpark.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.said.gestionpark.dao.IVehiculeDao;
import com.said.gestionpark.entites.Vehicule;
import com.said.gestionpark.service.IVehiculeService;

@Transactional
public class VehiculeServiceImpl implements IVehiculeService{
	
	private IVehiculeDao dao;
	
	public void setDao(IVehiculeDao dao) {
		this.dao = dao;
	}

	@Override
	public Vehicule save(Vehicule entity) {
		return dao.save(entity);
	}

	@Override
	public Vehicule update(Vehicule entity) {
		return dao.update(entity);
	}

	@Override
	public List<Vehicule> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Vehicule> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Vehicule getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Vehicule findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Vehicule findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	

}
