package com.said.gestionpark.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import com.said.gestionpark.dao.IEntretienDao;
import com.said.gestionpark.entites.Entretien;
import com.said.gestionpark.service.IEntretienService;

@Transactional
public class EntretienServiceImpl implements IEntretienService{
	
	private IEntretienDao dao;
	
	public void setDao(IEntretienDao dao) {
		this.dao = dao;
	}

	@Override
	public Entretien save(Entretien entity) {
		return dao.save(entity);
	}

	@Override
	public Entretien update(Entretien entity) {
		return dao.update(entity);
	}

	@Override
	public List<Entretien> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Entretien> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Entretien getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Entretien findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Entretien findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}


}
