package com.said.gestionpark.service.impl;

import java.util.List;

import com.said.gestionpark.dao.IConducteurDao;
import com.said.gestionpark.entites.Conducteur;
import com.said.gestionpark.service.IConducteurService;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public class ConducteurServiceImpl implements IConducteurService{
	
private IConducteurDao dao;
	
	public void setDao(IConducteurDao dao) {
		this.dao = dao;
	}

	@Override
	public Conducteur save(Conducteur entity) {
		return dao.save(entity);
	}

	@Override
	public Conducteur update(Conducteur entity) {
		return dao.update(entity);
	}

	@Override
	public List<Conducteur> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Conducteur> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Conducteur getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Conducteur findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Conducteur findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}


}
