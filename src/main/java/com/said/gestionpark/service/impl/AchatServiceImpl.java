package com.said.gestionpark.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.said.gestionpark.dao.IAchatDao;
import com.said.gestionpark.entites.Achat;
import com.said.gestionpark.service.IAchatService;


@Transactional
public class AchatServiceImpl implements IAchatService{
	
private IAchatDao dao;
	
	public void setDao(IAchatDao dao) {
		this.dao = dao;
	}

	@Override
	public Achat save(Achat entity) {
		return dao.save(entity);
	}

	@Override
	public Achat update(Achat entity) {
		return dao.update(entity);
	}

	@Override
	public List<Achat> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Achat> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Achat getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Achat findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Achat findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}


}
