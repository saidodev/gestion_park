package com.said.gestionpark.service;

import java.util.List;

import com.said.gestionpark.entites.Vehicule;


public interface IVehiculeService {
	
public Vehicule save(Vehicule entity);
	
	public Vehicule update(Vehicule entity);

	public List<Vehicule> selectAll();

	public List<Vehicule> selectAll(String sortField, String sort);

	public Vehicule getById(Long id);

	public void remove(Long id);

	public Vehicule findOne(String paramName, Object paramValue);

	public Vehicule findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
