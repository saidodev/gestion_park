package com.said.gestionpark.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.said.gestionpark.entites.Conducteur;
import com.said.gestionpark.entites.Departement;
import com.said.gestionpark.entites.Ville;
import com.said.gestionpark.service.IDepartementService;


@Controller
@RequestMapping(value="/departement")
public class DepartementController {
	
	@Autowired	
	private IDepartementService departementService;
	
	//get department//
	
		@RequestMapping(value = "/")
		public String departement(Model model) {
			
			List<Departement> departements = departementService.selectAll();
			if (departements == null) {
				departements = new ArrayList<Departement>();
			}
			model.addAttribute("departements", departements);
			return "departement/departement";
		}
		
		@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
		public String ajouterDepartement(Model model ) {
			Departement departement = new Departement();
			
		
			model.addAttribute("departement", departement);
			
			return "departement/ajouterDepartement";
		}
		
		//Save//
		@RequestMapping(value = "/enregistrer")
		  public String enregistrerConducteur(Model model, Departement departement) {
			
			if(departement != null)	{
			if (departement.getId_departement() != null) {
				departementService.update(departement);
			} 
			else
			{
				departementService.save(departement);
			}
			}
			
			return "redirect:/departement/";
		}
		
		
	//Delete
		
		@RequestMapping(value = "/supprimer/{id_departement}")
		
		public String supprimerDepartement(Model model, @PathVariable Long id_departement) {
			if (id_departement != null) {
				Departement departement = departementService.getById(id_departement);
				if (departement != null) {
					//TODO Verification avant suppression
					departementService.remove(id_departement);
				}
			}
			return "redirect:/departement/";
		
		}
		
		//MODIFIER
		
		@RequestMapping(value = "/modifier/{id_departement}")
		public String modifierDepartement(Model model, @PathVariable Long id_departement) {
			if (id_departement != null) {
				Departement departement = departementService.getById(id_departement);
				if (departement != null) {
					model.addAttribute("departement", departement);
				}
			}
			return "departement/ajouterDepartement";
		}
		
		

	

}
