package com.said.gestionpark.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.said.gestionpark.entites.Achat;
import com.said.gestionpark.entites.Fournisseur;
import com.said.gestionpark.entites.Vehicule;
import com.said.gestionpark.service.IAchatService;
import com.said.gestionpark.service.IFournisseurService;
import com.said.gestionpark.service.IVehiculeService;

@Controller
@RequestMapping(value="/achat")
public class AchatController {
	

	@Autowired	
	private IAchatService achatService;
	

	@Autowired	
	private IFournisseurService fournisseurService;
	

	@Autowired	
	private IVehiculeService vehiculeService;

//get Achat

	@RequestMapping(value = "/")
	public String achat(Model model) {
		
		List<Achat> achats = achatService.selectAll();
		if (achats == null) {
			achats = new ArrayList<Achat>();
		}
		model.addAttribute("achats", achats);
		return "achat/achat";
	}
	
	//Nouveau Achat//
	
	@RequestMapping(value="/nouveau",method = RequestMethod.GET)
	public String nouveauAchat(Model model)
	{
		Achat achat = new Achat();
		
		List<Fournisseur> fournisseurs = fournisseurService.selectAll();
		if (fournisseurs == null) {
			fournisseurs = new ArrayList<Fournisseur>();
		}
		
		List<Vehicule> vehicules = vehiculeService.selectAll();
		if (vehicules == null) {
			vehicules = new ArrayList<Vehicule>();
		}
		
		model.addAttribute("fournisseurs", fournisseurs);
		model.addAttribute("vehicules", vehicules);
		model.addAttribute("achat", achat);
		return "achat/ajouterAchat";
	}
	
	//Enregistrer un achat//
	
	@RequestMapping(value = "/enregistrer")
	  public String enregistrerConducteur(Model model, Achat achat) {
		
		if(achat != null)	{
		if (achat.getId_achat() != null) {
			achatService.update(achat);
		} 
		else
		{
			achatService.save(achat);
		}
		}
		
		return "redirect:/achat/";
	}
	
	//MODIFIER//
		@RequestMapping(value = "/modifier/{id_achat}")
		public String supprimerConducteur(Model model, @PathVariable Long id_achat) {
			
			if (id_achat != null) {
				Achat achat = achatService.getById(id_achat);
				
				List<Fournisseur> fournisseurs = fournisseurService.selectAll();
				if (fournisseurs == null) {
					fournisseurs = new ArrayList<Fournisseur>();
				}
				
				List<Vehicule> vehicules = vehiculeService.selectAll();
				if (vehicules == null) {
					vehicules = new ArrayList<Vehicule>();
				}
				
				model.addAttribute("fournisseurs", fournisseurs);
				model.addAttribute("vehicules", vehicules);
				
				if (achat != null) {
					model.addAttribute("achat", achat);
				}
			}
			
			
			return "achat/ajouterAchat";
		}
		
		//SUPPRIMER//
		@RequestMapping(value = "/supprimer/{id_achat}")
		public String modifierConducteur(Model model, @PathVariable Long id_achat) {
			if (id_achat != null) {
				Achat achat = achatService.getById(id_achat);
				if (achat != null) {
					//TODO Verification avant suppression
					achatService.remove(id_achat);
				}
			}
			return "redirect:/achat/";
		}
		
	
		@InitBinder
		public void initBinder(WebDataBinder binder) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			dateFormat.setLenient(false);
			binder.registerCustomEditor(Date.class, new CustomDateEditor(
					dateFormat, false));
		}


}
