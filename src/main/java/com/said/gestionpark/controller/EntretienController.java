package com.said.gestionpark.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.said.gestionpark.entites.Entretien;
import com.said.gestionpark.entites.Fournisseur;
import com.said.gestionpark.entites.Vehicule;
import com.said.gestionpark.service.IEntretienService;
import com.said.gestionpark.service.IFournisseurService;
import com.said.gestionpark.service.IVehiculeService;


@Controller
@RequestMapping(value="/entretien")
public class EntretienController {
	
	@Autowired	
	private IEntretienService entretienService;
	
	@Autowired	
	private IFournisseurService fournisseurService;
	
	@Autowired	
	private IVehiculeService vehiculeService;
	
	//Get Entretien
	
	@RequestMapping(value = "/")
	public String entretien(Model model) {
		
		List<Entretien> entretien = entretienService.selectAll();
		if (entretien == null) {
			entretien = new ArrayList<Entretien>();
		}
		model.addAttribute("entretiens", entretien);
		return "entretien/entretien";
	}
	
	
	//New
	
	@RequestMapping(value="/nouveau",method = RequestMethod.GET)
	public String nouveauEntretien(Model model)
	{
		Entretien entretien = new Entretien();
		
		List<Fournisseur> fournisseurs = fournisseurService.selectAll();
		if (fournisseurs == null) {
			fournisseurs = new ArrayList<Fournisseur>();
		}
		
		List<Vehicule> vehicules = vehiculeService.selectAll();
		if (vehicules == null) {
			vehicules = new ArrayList<Vehicule>();
		}
		
		model.addAttribute("fournisseurs", fournisseurs);
		model.addAttribute("vehicules", vehicules);
		model.addAttribute("entretien", entretien);
		return "entretien/ajouterEntretien";
	}
	

	  //Save//
	
		@RequestMapping(value = "/enregistrer")
		  public String enregistrerEntretien(Model model, Entretien entretien) {
			
			if(entretien != null)	{
			if (entretien.getId_entretien() != null) {
				entretienService.update(entretien);
			} 
			else
			{
				entretienService.save(entretien);
			}
			}
			
			return "redirect:/entretien/";
		}
		
		
		//MODIFIER//
				@RequestMapping(value = "/modifier/{id_entretien}")
				public String supprimerEntretien(Model model, @PathVariable Long id_entretien) {
					
					if (id_entretien != null) {
						Entretien entretien = entretienService.getById(id_entretien);
						
						List<Fournisseur> fournisseurs = fournisseurService.selectAll();
						if (fournisseurs == null) {
							fournisseurs = new ArrayList<Fournisseur>();
						}
						
						List<Vehicule> vehicules = vehiculeService.selectAll();
						if (vehicules == null) {
							vehicules = new ArrayList<Vehicule>();
						}
						
						model.addAttribute("fournisseurs", fournisseurs);
						model.addAttribute("vehicules", vehicules);
						
						if (entretien != null) {
							model.addAttribute("entretien", entretien);
						}
					}
					
					
					return "entretien/ajouterEntretien";
				}
				
				//SUPPRIMER//
				@RequestMapping(value = "/supprimer/{id_entretien}")
				public String modifierConducteur(Model model, @PathVariable Long id_entretien) {
					if (id_entretien != null) {
						Entretien entretien = entretienService.getById(id_entretien);
						if (entretien != null) {
							//TODO Verification avant suppression
							entretienService.remove(id_entretien);
						}
					}
					return "redirect:/entretien/";
				}
				
			
				@InitBinder
				public void initBinder(WebDataBinder binder) {
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					dateFormat.setLenient(false);
					binder.registerCustomEditor(Date.class, new CustomDateEditor(
							dateFormat, false));
				}
}
