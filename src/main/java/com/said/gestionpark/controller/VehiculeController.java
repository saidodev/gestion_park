package com.said.gestionpark.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.said.gestionpark.entites.Conducteur;
import com.said.gestionpark.entites.Departement;
import com.said.gestionpark.entites.Vehicule;
import com.said.gestionpark.entites.Ville;
import com.said.gestionpark.service.IConducteurService;
import com.said.gestionpark.service.IDepartementService;
import com.said.gestionpark.service.IVehiculeService;

@Controller
@RequestMapping(value="/vehicule")
	public class VehiculeController {
	

		@Autowired	
		private IVehiculeService vehiculeService;
		
		@Autowired	
		private IConducteurService conducteurService;
		

		@Autowired	
		private IDepartementService departementService;
		
		//get client
		
			@RequestMapping(value = "/")
			public String vehicule(Model model) {
				
				List<Vehicule> vehicule = vehiculeService.selectAll();
				if (vehicule == null) {
					vehicule = new ArrayList<Vehicule>();
				}
				model.addAttribute("vehicules", vehicule);
				return "vehicule/vehicule";
			}
			
			
			//New//
			
			@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
			public String ajouterVehicule(Model model ) {
				Vehicule vehicule = new Vehicule();
				
				List<Conducteur> conducteurs = conducteurService.selectAll();
				if (conducteurs == null) {
					conducteurs = new ArrayList<Conducteur>();
				}
				
				List<Departement> departements = departementService.selectAll();
				if (departements == null) {
					departements = new ArrayList<Departement>();
				}
				
				model.addAttribute("vehicule", vehicule);
				model.addAttribute("conducteurs", conducteurs);
				model.addAttribute("departements", departements);
				
				return "vehicule/ajouterVehicule";
			}
			
			
			 //Save//
			@RequestMapping(value = "/enregistrer")
			  public String enregistrerVehicule(Model model, Vehicule vehicule) {
				
				if(vehicule != null)	{
				if (vehicule.getId_vehicule() != null) {
					vehiculeService.update(vehicule);
				} 
				else
				{
					vehiculeService.save(vehicule);
				}
				}
				
				return "redirect:/vehicule/";
			}
			
			
			
			//MODIFIER//
			@RequestMapping(value = "/modifier/{id_vehicule}")
			public String supprimerConducteur(Model model, @PathVariable Long id_vehicule) {
				
				if (id_vehicule != null) {
					Vehicule vehicule = vehiculeService.getById(id_vehicule);
				;
					
					List<Conducteur> conducteurs = conducteurService.selectAll();
					if (conducteurs == null) {
						conducteurs = new ArrayList<Conducteur>();
					}
					model.addAttribute("conducteurs", conducteurs);
					
					
					List<Departement> departements = departementService.selectAll();
					if (departements == null) {
						departements = new ArrayList<Departement>();
					}
					model.addAttribute("departements", departements);
					
					
					if (vehicule != null) {
						model.addAttribute("vehicule", vehicule);
					}
				}
				
				
				return "vehicule/ajouterVehicule";
			}
			
			
			//SUPPRIMER//
			@RequestMapping(value = "/supprimer/{id_vehicule}")
			public String supprimerVehicule(Model model, @PathVariable Long id_vehicule) {
				if (id_vehicule != null) {
					Vehicule vehicule = vehiculeService.getById(id_vehicule);
					if (vehicule != null) {
						//TODO Verification avant suppression
						vehiculeService.remove(id_vehicule);
					}
				}
				return "redirect:/vehicule/";
			}
			
			
			@InitBinder
			public void initBinder(WebDataBinder binder) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				dateFormat.setLenient(false);
				binder.registerCustomEditor(Date.class, new CustomDateEditor(
						dateFormat, false));
			}
		
		}
