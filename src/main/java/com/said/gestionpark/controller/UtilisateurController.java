package com.said.gestionpark.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.said.gestionpark.entites.Roles;
import com.said.gestionpark.entites.Utilisateur;
import com.said.gestionpark.service.IRoleService;
import com.said.gestionpark.service.IUtilisateurService;

@Controller
@RequestMapping(value="/utilisateur")
public class UtilisateurController {
	
	
	@Autowired	
	private IUtilisateurService utilisateurService;
	
	@Autowired	
	private IRoleService roleService;
	//Get Entretien
	
		@RequestMapping(value = "/")
		public String entretien(Model model) {
			
			List<Utilisateur> utilisateur = utilisateurService.selectAll();
			if (utilisateur == null) {
				utilisateur = new ArrayList<Utilisateur>();
			}
			model.addAttribute("utilisateurs", utilisateur);
			return "utilisateur/utilisateur";
		}
		
		//New//
		
		@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
		public String ajouterUtilisateur(Model model ) {
			Utilisateur utilisateur = new Utilisateur();
			
			List<Roles> roles = roleService.selectAll();
			if (roles == null) {
				roles = new ArrayList<Roles>();
			}
		
			model.addAttribute("utilisateur", utilisateur);
			model.addAttribute("roles", roles);
		
			
			return "utilisateur/ajouterUtilisateur";
		}
		
		 //Save//
		@RequestMapping(value = "/enregistrer")
		  public String enregistrerUtilisateur(Model model, Utilisateur utilisateur) {
			
			if(utilisateur != null)	{
			if (utilisateur.getIdUtilisateur() != null) {
				utilisateurService.update(utilisateur);
			} 
			else
			{
				//Encryption password//
				/*BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
				String password =passwordEncoder.encode(utilisateur.getMotDePasse());
				utilisateur.setMotDePasse(password);*/
				
								
				utilisateurService.save(utilisateur);
			}
			}
			
			return "redirect:/utilisateur/";
		}
		
		
		//MODIFIER//
		@RequestMapping(value = "/modifier/{idUtilisateur}")
		public String supprimerConducteur(Model model, @PathVariable Long idUtilisateur) {
			
			if (idUtilisateur != null) {
				Utilisateur utilisateur = utilisateurService.getById(idUtilisateur);
		
			List<Roles> roles = roleService.selectAll();
			if (roles == null) {
				roles = new ArrayList<Roles>();
			}
			model.addAttribute("roles", roles);	
			
			if (utilisateur != null) {
					model.addAttribute("utilisateur", utilisateur);
				}
			}
			
			
			return "utilisateur/ajouterUtilisateur";
		}
		
		
		//SUPPRIMER//
		@RequestMapping(value = "/supprimer/{idUtilisateur}")
		public String supprimerVehicule(Model model, @PathVariable Long idUtilisateur) {
			if (idUtilisateur != null) {
				Utilisateur utilisateur = utilisateurService.getById(idUtilisateur);
				if (utilisateur != null) {
				
					utilisateurService.remove(idUtilisateur);
				}
			}
			return "redirect:/utilisateur/";
		}
		

}
