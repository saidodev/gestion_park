package com.said.gestionpark.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.said.gestionpark.entites.Conducteur;
import com.said.gestionpark.entites.Fournisseur;
import com.said.gestionpark.entites.Ville;
import com.said.gestionpark.service.IFournisseurService;
import com.said.gestionpark.service.IVilleService;

@Controller
@RequestMapping(value="/fournisseur")
public class FournisseurController {


@Autowired	
private IFournisseurService fournisseurService;

@Autowired
private IVilleService villeService;

	@RequestMapping(value = "/")
	public String fournisseur(Model model) {
		
		List<Fournisseur> fournisseurs = fournisseurService.selectAll();
		if (fournisseurs == null) {
			fournisseurs = new ArrayList<Fournisseur>();
		}
		model.addAttribute("fournisseurs", fournisseurs);
		return "fournisseur/fournisseur";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterFournisseur(Model model ) {
		Fournisseur fournisseur = new Fournisseur();

		List<Ville> villes = villeService.selectAll();
		if (villes == null) {
			villes = new ArrayList<Ville>();
		}
		model.addAttribute("fournisseur", fournisseur);
		model.addAttribute("villes", villes);
		return "fournisseur/ajouterFournisseur";
	}
	
	//Save//
		@RequestMapping(value = "/enregistrer")
		  public String enregistrerFournisseur(Model model, Fournisseur fournisseur) {
			
			if(fournisseur != null)	{
			if (fournisseur.getId_fournisseur() != null) {
				fournisseurService.update(fournisseur);
			} 
			else
			{
				fournisseurService.save(fournisseur);
			}
			}
			
			return "redirect:/fournisseur/";
		}
		
		//MODIFIER//
		@RequestMapping(value = "/modifier/{id_fournisseur}")
		public String modifierFournisseur(Model model, @PathVariable Long id_fournisseur) {
			
			if (id_fournisseur != null) {
				Fournisseur fournisseur = fournisseurService.getById(id_fournisseur);
				List<Ville> villes = villeService.selectAll();
				if (villes == null) {
					villes = new ArrayList<Ville>();
				}
				model.addAttribute("villes", villes);
				if (fournisseur != null) {
					model.addAttribute("fournisseur", fournisseur);
				}
			}
			
			
			return "fournisseur/ajouterFournisseur";
		}
		
		//SUPPRIMER//
		@RequestMapping(value = "/supprimer/{id_fournisseur}")
		public String supprimerFournisseur(Model model, @PathVariable Long id_fournisseur) {
			if (id_fournisseur != null) {
				Fournisseur fournisseur = fournisseurService.getById(id_fournisseur);
				if (fournisseur != null) {
					//TODO Verification avant suppression
					fournisseurService.remove(id_fournisseur);
				}
			}
			return "redirect:/fournisseur/";
		}

}
