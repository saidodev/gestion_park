package com.said.gestionpark.controller;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.said.gestionpark.entites.Conducteur;
import com.said.gestionpark.entites.Ville;
import com.said.gestionpark.service.IConducteurService;
import com.said.gestionpark.service.IVilleService;

@Controller
@RequestMapping(value="/conducteur")

public class ConducteurController {
	
@Autowired	
private IConducteurService conducteurService;

@Autowired
private IVilleService villeService;
	
	
	//get client
	
	@RequestMapping(value = "/")
	public String client(Model model) {
		
		List<Conducteur> conducteurs = conducteurService.selectAll();
		if (conducteurs == null) {
			conducteurs = new ArrayList<Conducteur>();
		}
		model.addAttribute("conducteurs", conducteurs);
		return "conducteur/conducteur";
	}
	
	//New//
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterClient(Model model ) {
		Conducteur conducteur = new Conducteur();
		
		List<Ville> villes = villeService.selectAll();
		if (villes == null) {
			villes = new ArrayList<Ville>();
		}
		model.addAttribute("conducteur", conducteur);
		model.addAttribute("villes", villes);
		return "conducteur/ajouterConducteur";
	}
	
	
   //Save//
	@RequestMapping(value = "/enregistrer")
	  public String enregistrerConducteur(Model model, Conducteur conducteur) {
		
		if(conducteur != null)	{
		if (conducteur.getId_conducteur() != null) {
			conducteurService.update(conducteur);
		} 
		else
		{
			conducteurService.save(conducteur);
		}
		}
		
		return "redirect:/conducteur/";
	}
	
	
	
	//MODIFIER//
	@RequestMapping(value = "/modifier/{id_conducteur}")
	public String supprimerConducteur(Model model, @PathVariable Long id_conducteur) {
		
		if (id_conducteur != null) {
			Conducteur conducteur = conducteurService.getById(id_conducteur);
			List<Ville> villes = villeService.selectAll();
			if (villes == null) {
				villes = new ArrayList<Ville>();
			}
			model.addAttribute("villes", villes);
			if (conducteur != null) {
				model.addAttribute("conducteur", conducteur);
			}
		}
		
		
		return "conducteur/ajouterConducteur";
	}
	
	//SUPPRIMER//
	@RequestMapping(value = "/supprimer/{id_conducteur}")
	public String modifierConducteur(Model model, @PathVariable Long id_conducteur) {
		if (id_conducteur != null) {
			Conducteur conducteur = conducteurService.getById(id_conducteur);
			if (conducteur != null) {
				//TODO Verification avant suppression
				conducteurService.remove(id_conducteur);
			}
		}
		return "redirect:/conducteur/";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

}
