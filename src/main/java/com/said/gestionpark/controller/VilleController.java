package com.said.gestionpark.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.said.gestionpark.entites.Conducteur;
import com.said.gestionpark.entites.Ville;
import com.said.gestionpark.service.IVilleService;

@Controller
@RequestMapping(value="/ville")
public class VilleController {

	@Autowired	
	private IVilleService villeService;
	
	//get department//
	
		@RequestMapping(value = "/")
		public String ville(Model model) {
			List<Ville> villes = villeService.selectAll();
			if (villes == null) {
				villes = new ArrayList<Ville>();
			}
			model.addAttribute("villes", villes);
			
			return "ville/ville";
		}
		
		
	//New 
		
		@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
		public String ajouterVille(Model model ) {
			Ville villes = new Ville();
			model.addAttribute("ville", villes);
	
			return "ville/ajouterVille";
		}
		
	//Save
		
		@RequestMapping(value = "/enregistrer")
		  public String enregistrerVille(Model model, Ville ville) {
			
			if(ville != null)	{
				
					if (ville.getId_ville() != null) {
						villeService.update(ville);
						System.out.println("Modifier Ville");
					} 
					else
					{
						System.out.println("Ajouter Ville");
						villeService.save(ville);
					}
			System.out.println("non Ville");
			}
		
			
			return "redirect:/ville/";
		}
		
		//Edit
		
		@RequestMapping(value = "/modifier/{id_ville}")
		public String modifierVille(Model model, @PathVariable Long id_ville) {
			
			if (id_ville != null) {
				Ville ville = villeService.getById(id_ville);
				if (ville != null) {
					model.addAttribute("ville", ville);
				}
			}
			return "ville/ajouterVille";
			
			
		}
		
		//Delete
		
		@RequestMapping(value = "/supprimer/{id_ville}")
		
		public String supprimerVille(Model model, @PathVariable Long id_ville) {
			if (id_ville != null) {
				Ville ville = villeService.getById(id_ville);
				if (ville != null) {
					List<Conducteur> conducteurVille = villeService.selectAllConducteurByVille(id_ville);
					if (conducteurVille.isEmpty() ) {
						villeService.remove(id_ville);
					} else {
						return "Impossible de supprimer cette cat�gprie, car elle est affect� � des articles";
					}
				}
			}
			return "redirect:/ville/";
		}
		
		
		
}
