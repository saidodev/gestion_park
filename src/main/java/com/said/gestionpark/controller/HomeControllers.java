package com.said.gestionpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/home")


public class HomeControllers {
	
	@RequestMapping(value="/")
	public String home() {
		return "home/home";
	}
	
	@RequestMapping(value="/blank")
	
	public String blankHome() {
		return "blank/blank";
	}
	
	

}
