package com.said.gestionpark.dao;

import com.said.gestionpark.entites.Departement;

public interface IDepartementDao extends IGenericDao<Departement>{

}
