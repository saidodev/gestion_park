package com.said.gestionpark.dao;

import com.said.gestionpark.entites.Vehicule;

public interface IVehiculeDao extends IGenericDao<Vehicule> {

}
