package com.said.gestionpark.dao;

import java.util.List;

import com.said.gestionpark.entites.Conducteur;
import com.said.gestionpark.entites.Ville;

public interface IVilleDao extends IGenericDao<Ville> {
	
	public List<Conducteur> selectAllConducteurByVille(Long id_ville);

}
