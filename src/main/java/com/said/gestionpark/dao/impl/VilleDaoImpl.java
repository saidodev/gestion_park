package com.said.gestionpark.dao.impl;

import java.util.List;

import javax.persistence.Query;

import com.said.gestionpark.dao.IVilleDao;
import com.said.gestionpark.entites.Conducteur;
import com.said.gestionpark.entites.Ville;

public class VilleDaoImpl extends GenericDaoImpl<Ville> implements IVilleDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<Conducteur> selectAllConducteurByVille(Long id_ville) {
		
		Query query = em.createQuery("select a from " + Conducteur.class.getSimpleName() + " a where a.ville.id_ville = :x");
		query.setParameter("x", id_ville);
		return query.getResultList();
		
	}

}
