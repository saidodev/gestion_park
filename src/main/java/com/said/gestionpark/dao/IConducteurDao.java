package com.said.gestionpark.dao;

import com.said.gestionpark.entites.Conducteur;

public interface IConducteurDao extends IGenericDao<Conducteur>{

}
