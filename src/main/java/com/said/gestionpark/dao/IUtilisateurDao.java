package com.said.gestionpark.dao;

import com.said.gestionpark.entites.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur> {

}
